package tdd;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Victoria on 12.02.2017.
 */
public class MamkaEbunTest {

    @Test
    public void test() {

        String state = "neebana";
        Mamka mamka = new Mamka(state);

        MamkaEbun mamkaEbun = new MamkaEbun();

        mamkaEbun.ebi(mamka);

        Assert.assertEquals(mamka.getState(), "viebana");
    }

}
