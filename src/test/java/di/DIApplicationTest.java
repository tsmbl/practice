package di;

import com.google.inject.AbstractModule;
import com.google.inject.util.Modules;
import org.junit.Before;

import static org.junit.Assert.*;

/**
 * Created by Victoria on 05.03.2017.
 */
public class DIApplicationTest {

    @Before
    public void before() {

        Modules.override(new AppicationGuiceModule(), new AbstractModule() {
            @Override
            protected void configure() {
                bind(DataBaseFacade.class).toInstance(new DataBaseFacade() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }
                });
            }
        });

    }
}