package oop;

import java.io.IOException;
import java.util.Scanner;

public class Calculate{
    public Scanner scanner = new Scanner(System.in);
    Output output = new Output();

    public static Volume do_calc(){
        return new Volume();
    }

    public int get_int_val() throws IOException{
            int i = scanner.nextInt();
        if (i > 0) {
            return i;
        } else throw new NumberFormatException("All the values must be positive");
    }

    public double get_double_val() throws IOException{
        double i = scanner.nextDouble();
        if (i > 0) {
            return i;
        } else throw new NumberFormatException("All the values must be positive");
    }
}

class Output{
    public void display(double n){
        System.out.printf("%.3f%n", n);
    }
}

class Volume{
    public double get_volume(int a){ //volume of cube
        return a * a * a;
    }
    public double get_volume(int l, int b, int h){ //volume of cuboid
        return l * b * h;
    }
    public double get_volume(double r){ //volume of hemisphere
        return ((double) 2/3) * Math.PI * r * r * r;
    }
    public double get_volume(double r, double h){
        return Math.PI * r * r * h;
    }

}