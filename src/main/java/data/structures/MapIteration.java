package data.structures;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Victoria on 09.02.2017.
 */
public class MapIteration {

    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        System.out.println("Using entrySet:");
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
        System.out.println();

        System.out.println("Using keySet:");
        for (Integer key : map.keySet()) {
            System.out.println(key + " = " + map.get(key));
        }
        System.out.println();

        System.out.println("Using values (can't get keys):");
        for (String value : map.values()) {
            System.out.println(value);
        }
        System.out.println();

        //stream api - слишком сложно, просто ебать сложно
    }

}


