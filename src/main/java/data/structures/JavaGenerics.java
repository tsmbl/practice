package data.structures;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Victoria on 09.02.2017.
 */
public class JavaGenerics {
    class Printer<T> {
        void printArray(T[] arr) {

            Arrays.stream(arr)
                    .forEach(System.out::println);

            for (T elem : arr) {
                System.out.println(elem);
            }
        }
    }
}
