package data.structures;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 * Created by Victoria on 23.01.2017.
 */

public class JavaList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List list = new ArrayList();
        int n = scanner.nextInt();
/*        // Old style collections now known as raw types
        List aList = new ArrayList(); //Could contain anything
        // New style collections with Generics
        List<String> aList = new ArrayList<String>(); //Contains only Strings

        В общем, стараются использовать типизацию, где это возможно http://stackoverflow.com/a/2770338/4913894

        */
        for (int i = 0; i < n; i++) {
            list.add(scanner.nextInt());
        }
        int queries = scanner.nextInt();
        int count = 0;
        for (int t = 0; t < queries; t++){
            System.out.println(count++);
            String s = scanner.next();
            if(s.equals("Delete")){
                list.remove(scanner.nextInt());
            } else if(s.equals("Insert")){
               list.add(scanner.nextInt(), scanner.nextInt());
            }
        }
        System.out.println(list);
    }
}
