package data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class ListAggregator implements Iterable {

    /*   это правильно, но небольшой комментарий по клин-коду:
       лучше везде программировать на уровне
       интерфейсов, вот почему:
       http://stackoverflow.com/questions/9852831/polymorphism-why-use-list-list-new-arraylist-instead-of-arraylist-list-n
       т.е., например, если мы создаем очередь, то ее можно объявить как
       PriorityQueue<Student> students = new PriorityQueue(),
       а можно как Queue<Student> students = new PriorityQueue()
       обычно используют второй вариант по той же причине, что в ссылке*/
    private List<String> stringList = new ArrayList<String>();

    private List<Integer> intList = new ArrayList<Integer>();

    public static void main(String[] args) {
        ListAggregator la = new ListAggregator();
        la.addString("one");
        la.addString("two");
        la.addInt(1);
        la.addInt(2);
        la.addString("three");
        la.addString("four");
        la.addInt(3);
        la.addInt(4);
        la.addInt(5);
        for (Object obj : la) {
            System.out.println(obj);
        }
    }

    public Iterator iterator() {
        return new ListAggregatorIterator();
    }

    public void addString(String s) {
        stringList.add(s);
    }

    public void addInt(Integer i) {
        intList.add(i);
    }

    private class ListAggregatorIterator implements Iterator {
        int cursor;

        int lastRet = -1;

        public boolean hasNext() {
            return cursor != stringList.size() + intList.size();
        }

        public Object next() {
            int i = cursor;
            if (i >= stringList.size() + intList.size()) {
                throw new NoSuchElementException();
            }
            if (i < stringList.size()) {
                lastRet = i;
                cursor = i + 1;
                return stringList.get(i);
            } else {
                lastRet = i;
                cursor = i + 1;
                return intList.get(i - stringList.size());
            }
        }

        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException();
/*         нормал, хорошая проверка, в начале метода часто вводят подобные (проверка индесов,
         проверка, что аргумент != null), см. комментарий дальше
*/
            }
            if (lastRet < stringList.size()) {
                try {
                    stringList.remove(lastRet);
                    cursor = lastRet;
                    lastRet = -1;
                } catch (IndexOutOfBoundsException ex) {
                    // лучше не оставлять пустые catch блоки, т.к. они скрывают ошибку, ее можно не увидеть
                    // -- хорошим способом избежеать этого кэтч блока будет проверка индексов, см. метод
                    // ArrayList.remove(int index)
                    // -- обычно при возникновении ошибки ее стараются логгировать (например, вывести
                    // сообщение об ошибке в консоль)
                }
            } else {
                try {
                    intList.remove(lastRet - stringList.size());
                    cursor = lastRet;
                    lastRet = -1;
                } catch (IndexOutOfBoundsException ex) {
                    // лучше не оставлять пустые catch блоки, т.к. они скрывают ошибку, ее можно не увидеть
                    // -- хорошим способом избежеать этого кэтч блока будет проверка индексов, см. метод
                    // ArrayList.remove(int index)
                    // -- обычно при возникновении ошибки ее стараются логгировать (например, вывести
                    // сообщение об ошибке в консоль)
                }
            }
        }
    }
}
