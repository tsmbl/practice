package data.structures;

import java.util.*;
/**
 * Created by Victoria on 23.01.2017.
 */
public class JavaArrayList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
        int numOfLists = scanner.nextInt();
        for (int i = 0; i < numOfLists; i++) {
            int n = scanner.nextInt();
            ArrayList<Integer> sublist= new ArrayList<Integer>(n);
            for (int j = 0; j < n; j++){
                sublist.add(scanner.nextInt());
            }
            list.add(sublist);
        }
        int queries = scanner.nextInt();
        while (queries-- > 0){
            int number = scanner.nextInt()-1;
            int position = scanner.nextInt()-1;
            ArrayList<Integer> sublist = list.get(number);
            if(sublist.size()> position){
                System.out.println(sublist.get(position));
            } else {
                System.out.println("ERROR!");
            }
        }
    }
}
