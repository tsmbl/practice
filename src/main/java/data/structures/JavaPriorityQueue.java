package data.structures;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

class Student {
    private int token;
    private String fname;
    private double cgpa;

    public Student(String fname, double cgpa, int id) {
        super();
        this.token = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }

    public int getToken() {
        return token;
    }

    public String getFname() {
        return fname;
    }

    public double getCgpa() {
        return cgpa;
    }
}

public class JavaPriorityQueue {

    public static void main(String[] args) {
        Comparator<Student> studentComparator = new Comparator<Student>() {
            public int compare(Student s1, Student s2) {
                if (s1.getCgpa() > s2.getCgpa()) {
                    return -1;
                } else if (s1.getCgpa() < s2.getCgpa()) {
                    return 1;
                }

                int comparedValue = s1.getFname().compareTo(s2.getFname());
                if (comparedValue != 0) {
                    return comparedValue;
                }

                if (s1.getToken() < s2.getToken()) {
                    return 1;
                } else if (s1.getToken() > s2.getToken()) {
                    return -1;
                }
                return 0;
            }
        };
        Scanner in = new Scanner(System.in);
        int totalEvents = Integer.parseInt(in.nextLine());
        PriorityQueue<Student> students = new PriorityQueue<Student> (totalEvents, studentComparator);
        while (totalEvents-- > 0) {
            String event = in.next();
            if (event.equals("SERVED")) {
                students.poll();
            } else if (event.equals("ENTER")) {
                students.offer(new Student(in.next(), Double.parseDouble(in.next()), Integer.parseInt(in.next())));
            }
            if (in.hasNext()) in.nextLine();
        }

        if (students.isEmpty()) {
            System.out.println("EMPTY");
        } else {
            while (!students.isEmpty())
                System.out.println(students.poll().getFname());
        }
    }
}

