package data.structures;

/**
 * Created by Victoria on 23.01.2017.
 */
public class Subarray {
    public int subsums(int[] arr) {
        int total = 0;
        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            int count = i;
            while (count < arr.length) {
                sum += arr[count];
                if (sum < 0) total++;
                count++;
            }
        }
        return total;
    }

    public static void main(String[] args) {
        Subarray s = new Subarray();
        int[] arr = new int[]{1, -2, 4, -5, 1};
        System.out.println(s.subsums(arr));
    }
}

