package di;

import javax.inject.Inject;

/**
 * Created by Victoria on 05.03.2017.
 */
public class ClassC {

    private final DataFacade facade;
    @Inject
    public ClassC(DataFacade facade) {
        this.facade = facade;
    }
}
