package di;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class DataFacadeImpl2 implements DataFacade {

    private final Config config;

    private static final int CAPACITY = 100;
    private final Map<String, BlockingQueue<Object>> queues = new HashMap<>();


    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);

    @Inject
    public DataFacadeImpl2(Config config) {
        this.config = config;
    }


    @Override
    public void subscribe(Consumer<List<Object>> handler, String key) {
        executorService.scheduleAtFixedRate(() -> {
            BlockingQueue<Object> objects = queues.get(key);
            List<Object> buffer = Lists.newArrayList();
            try {
                Queues.drain(objects, buffer, objects.size(), 1L, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (buffer.size() > 0) {
                handler.accept(buffer);
            }

        }, 0L, 1L, TimeUnit.SECONDS);
    }

    @Override
    public void publish(Object message, String key) {
        queues.computeIfAbsent(key, k -> new ArrayBlockingQueue<>(CAPACITY))
                .add(message);

    }

    public static void main(String[] args) {


        List<Object> myObjects = Lists.newArrayList();

        DataFacadeImpl2 dataFacade = new DataFacadeImpl2(ConfigFactory.load());
        dataFacade.subscribe(objects -> myObjects.addAll(objects), "hui");

    }
}
