package di;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class DataFacadeImpl implements DataFacade {

    private static final int CAPACITY = 100;
    private final Map<String, BlockingQueue<Object>> queues = new HashMap<>();


    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);


    @Override
    public void subscribe(Consumer<List<Object>> handler, String key) {
        executorService.scheduleAtFixedRate(() -> {
            BlockingQueue<Object> objects = queues.get(key);
            List<Object> buffer = Lists.newArrayList();
            try {
                Queues.drain(objects, buffer, objects.size(), 1L, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (buffer.size() > 0) {
                handler.accept(buffer);
            }

        }, 0L, 1L, TimeUnit.SECONDS);
    }

    @Override
    public void publish(Object message, String key) {
        queues.computeIfAbsent(key, k -> new ArrayBlockingQueue<>(CAPACITY))
                .add(message);

    }

    public static void main(String[] args) {


        List<Object> myObjects = Lists.newArrayList();

        DataFacadeImpl dataFacade = new DataFacadeImpl();
        dataFacade.subscribe(objects -> myObjects.addAll(objects), "hui");

    }
}
