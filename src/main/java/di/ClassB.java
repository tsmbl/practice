package di;

import javax.inject.Inject;

/**
 * Created by Victoria on 05.03.2017.
 */
public class ClassB {

    private final ClassC classC;
    @Inject
    public ClassB(ClassC classC) {
        this.classC = classC;
    }
}
