package di;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * Created by Victoria on 05.03.2017.
 */
public class NoDIApplication {

    public static void main(String[] args) {


        DataFacadeImpl dataFacade = new DataFacadeImpl();

        Config cfg = ConfigFactory.load();


        ClassA classA = new ClassA(new ClassB(new ClassC(dataFacade)), cfg);
        ClassD classD = new ClassD(dataFacade, cfg);
    }
}
