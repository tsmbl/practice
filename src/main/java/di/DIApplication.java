package di;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Created by Victoria on 05.03.2017.
 */
public class DIApplication {

    public static void main(String[] args) {


        Injector injector = Guice.createInjector(new AppicationGuiceModule());

        ClassA classA = injector.getInstance(ClassA.class);
        ClassD classD = injector.getInstance(ClassD.class);
    }
}
