package di;

import com.typesafe.config.Config;

import javax.inject.Inject;

/**
 * Created by Victoria on 05.03.2017.
 */
public class ClassA {

    private final ClassB classB;

    private final Config config;

    @Inject
    public ClassA(ClassB classB, Config config) {
        this.classB = classB;
        this.config = config;
    }
}
