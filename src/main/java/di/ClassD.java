package di;

import com.typesafe.config.Config;

import javax.inject.Inject;

/**
 * Created by Victoria on 05.03.2017.
 */
public class ClassD {

    private final DataFacade dataFacade;
    private Config config;

    @Inject
    public ClassD(DataFacade dataFacade, final Config config) {
        this.dataFacade = dataFacade;
        this.config = config;
    }
}
