package di;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import javax.inject.Singleton;

/**
 * Created by Victoria on 05.03.2017.
 */
public class AppicationGuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(DataFacade.class).to(DataFacadeImpl2.class).in(Singleton.class);
        bind(DataBaseFacade.class).to(MySQL.class).in(Singleton.class);
    }

    @Provides
    @Singleton
    public Config config() {
        return ConfigFactory.load();
    }
}
