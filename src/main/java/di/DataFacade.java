package di;

import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Victoria on 05.03.2017.
 */
public interface DataFacade {


    void subscribe(final Consumer<List<Object>> handler, final String key);

    void publish(final Object message, final String key);
}