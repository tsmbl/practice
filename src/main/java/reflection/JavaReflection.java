package reflection;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Student {
    private String name;
    private String id;
    private String email;

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void anothermethod() {
    }
}

public class JavaReflection {

    public static void main(String[] args) {
        Class student = new Student().getClass();
        Method[] methods = student.getDeclaredMethods();

        List<Method> methodList = new ArrayList<>(Arrays.asList(methods));
        methodList
                .stream()
                .map(p -> p.getName())
                .sorted()
                .forEach(p -> System.out.println(p));
    }

}
