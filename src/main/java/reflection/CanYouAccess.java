package reflection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;


public class CanYouAccess {

    public static void main(String[] args) throws Exception {

        Object o;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine().trim());
        Class<CanYouAccess.Inner.Private> privateClass = CanYouAccess.Inner.Private.class;
        Constructor[] ctors = privateClass.getDeclaredConstructors();
        ctors[0].setAccessible(true);
        o = ctors[0].newInstance(new CanYouAccess.Inner());
        String str = privateClass.cast(o).powerof2(num);
        System.out.println(num + " is " + str);
        System.out.println("An instance of class: " + o.getClass().getCanonicalName() + " has been created");
    }//end of main

    static class Inner {
        private class Private {
            private String powerof2(int num) {
                return ((num & num - 1) == 0) ? "power of 2" : "not a power of 2";
            }
        }
    }//end of Inner

}//end of Solution


