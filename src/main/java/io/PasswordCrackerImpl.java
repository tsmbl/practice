package io;

import java.util.ArrayList;
import java.util.List;


public class PasswordCrackerImpl implements PasswordCracker {

    @Override
    public List<PasswordCrackingResult> crack(List<User> users) {

        List<PasswordCrackingResult> list = new ArrayList<>();
        SingleUserPasswordCracker singleCracker = new SingleUserPasswordCrackerImpl();
        users.stream().forEach(u -> list.add(singleCracker.crack(u)));
        return list;
    }
}
