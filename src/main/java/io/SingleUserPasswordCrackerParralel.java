package io;

import org.apache.commons.lang.RandomStringUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by Victoria on 23.02.2017.
 */
public class SingleUserPasswordCrackerParralel implements SingleUserPasswordCracker {
    @Override
    public PasswordCrackingResult crack(User user) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        AtomicInteger count = new AtomicInteger(0);
        String actualPassword = user.getPassword();

        CountDownLatch countDownLatch = new CountDownLatch(1);

        IntStream.range(0, 4)
                .mapToObj(ignored -> new PasswordBruteforce(count, actualPassword, countDownLatch))
                .forEach(executorService::execute);

        awaitPasswordIsFound(countDownLatch);
        executorService.shutdownNow();

        return new PasswordCrackingResultImpl(user, count.get());
    }

    private void awaitPasswordIsFound(CountDownLatch countDownLatch) {
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private static class PasswordBruteforce implements Runnable {

        private final AtomicInteger count;
        private final String actualPassword;
        private final CountDownLatch countDownLatch;

        public PasswordBruteforce(AtomicInteger count, String actualPassword, CountDownLatch countDownLatch) {

            this.count = count;
            this.actualPassword = actualPassword;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            String wannabePassword;
            do {
                wannabePassword = RandomStringUtils.randomAlphanumeric(4).toLowerCase();
                count.getAndIncrement();
            }
            while (!wannabePassword.equals(actualPassword) && !Thread.currentThread().isInterrupted());
            countDownLatch.countDown();

        }
    }
}
