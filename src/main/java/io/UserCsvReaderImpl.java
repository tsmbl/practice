package io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class UserCsvReaderImpl implements UserCsvReader {
    @Override
    public List<User> readFile(String file) {

        List<User> userList = new ArrayList<>();

        ClassLoader classLoader = getClass().getClassLoader();
        File users = new File(classLoader.getResource(file).getFile());
        try (Scanner scanner = new Scanner(users)) {
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                userList.add(stringToUser(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList;
    }

    private User stringToUser(String s) {
        String[] userInfo = s.split(",");
        User user = new Userino(Integer.parseInt(userInfo[0]), userInfo[1], userInfo[2]);
        return user;
    }

}
