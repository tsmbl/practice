package io.resources;

import com.codahale.metrics.annotation.Timed;
import io.PasswordCrackingResult;
import io.SingleUserPasswordCrackerImpl;
import io.User;
import io.Userino;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

/**
 * Created by Victoria on 07.03.2017.
 */

@Path("/password")
@Produces(MediaType.APPLICATION_JSON)
public class PasswordCrackingResultResource {

    @GET
    @Timed
    public PasswordCrackingResult crack(@QueryParam("id") Optional<Integer> id,
                                        @QueryParam("name") Optional<String> name,
                                        @QueryParam("password") Optional<String> password) {
        User user = new Userino(id.get(), name.get(), password.get());
        return new SingleUserPasswordCrackerImpl().crack(user);
    }
}
