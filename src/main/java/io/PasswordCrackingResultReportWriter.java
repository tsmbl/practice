package io;

import java.util.Collection;

@FunctionalInterface
public interface PasswordCrackingResultReportWriter {

    void writeReport(final Collection<PasswordCrackingResult> passwordCrackingResults);
}