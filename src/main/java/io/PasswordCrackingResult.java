package io;

/**
 * Result of password cracking -- contains user and number of attempts to crack password
 */
public interface PasswordCrackingResult {

  User getUser();

  int getNumberOfAttempts();

}
