package io;

import java.util.List;

@FunctionalInterface
public interface PasswordCracker {

  List<PasswordCrackingResult> crack(final List<User> users);
}
