package io;

/**
 * cracks password for single user
 */
@FunctionalInterface
public interface SingleUserPasswordCracker {

  PasswordCrackingResult crack(final User user);
}
