package io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import data.structures.JavaArrayList;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.resources.PasswordCrackingResultResource;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.List;

/**
 * Created by Victoria on 24.02.2017.
 */
public class PassCrackerApplication extends Application<Configuration> {
    List<User> list;
    static Config config = ConfigFactory.load();

    public void readUsers(){
        UserCsvReader s = new UserCsvReaderImpl();
        list = s.readFile("users.csv");
    }

    public List<PasswordCrackingResult> crackSingleThread(){
        readUsers();
        PasswordCracker cracker = new PasswordCrackerImpl();
        return cracker.crack(list);
    }

    public List<PasswordCrackingResult> crackMultiThread(){
        readUsers();
        PasswordCracker cracker = new PasswordCrackerParallel();
        return cracker.crack(list);

    }

    public void writeReportCsv(List<PasswordCrackingResult> results){
        PasswordCrackingResultReportWriter reportWriter = new PasswordCrackingResultReportWriterToCsv(config);
        reportWriter.writeReport(results);
    }
    public void writeReportJson(List<PasswordCrackingResult> results){
        PasswordCrackingResultReportWriter reportWriter = new PasswordCrackingResultReportWriterToJson(config);
        reportWriter.writeReport(results);
    }
    public void writeReportSQL(List<PasswordCrackingResult> results){
        PasswordCrackingResultReportWriter reportWriter = new PasswordCrackingResultReportWriterToSQL(config);
        reportWriter.writeReport(results);
    }

    public void writeReport(List<PasswordCrackingResult> results){
        try {
            List<String> classNames = config.getStringList("reportWriters");
            for (String className : classNames) {
                Class c = Class.forName(className);
                Constructor<?> ctor = c.getConstructor(Config.class);
                PasswordCrackingResultReportWriter reportWriter = (PasswordCrackingResultReportWriter) ctor.newInstance(config);
                reportWriter.writeReport(results);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initialize(Bootstrap<Configuration> b) {

        b.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
    }


    public void run(Configuration c, Environment e) throws Exception {
        e.jersey().register(new PasswordCrackingResultResource());
    }

    public static void main(String[] args) throws Exception{
//        PassCrackerApplication application = new PassCrackerApplication();
//        List<PasswordCrackingResult> results = application.crackMultiThread();
//        application.writeReport(results);
        new PassCrackerApplication().run(args);
    }
}
