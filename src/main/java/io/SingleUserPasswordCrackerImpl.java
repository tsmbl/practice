package io;

import org.apache.commons.lang.RandomStringUtils;

/**
 * Created by Victoria on 21.02.2017.
 */
public class SingleUserPasswordCrackerImpl implements SingleUserPasswordCracker {
    @Override
    public PasswordCrackingResult crack(User user) {
        int count = 0;
        String wannabePassword;
        String actualPassword = user.getPassword();
        do {
            wannabePassword = RandomStringUtils.randomAlphanumeric(4).toLowerCase();
            count++;
        }
        while (!wannabePassword.equals(actualPassword));
        return new PasswordCrackingResultImpl(user, count);
    }
}
