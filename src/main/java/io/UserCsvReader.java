package io;

import java.util.List;

/**
 * Reads users from file
 */
public interface UserCsvReader {

  List<User> readFile(final String file);
}
