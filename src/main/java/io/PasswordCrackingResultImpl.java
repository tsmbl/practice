package io;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Victoria on 21.02.2017.
 */
public class PasswordCrackingResultImpl implements PasswordCrackingResult {
    private User user;
    private int attempts;

    public PasswordCrackingResultImpl(User user, int attempts) {
        this.user = user;
        this.attempts = attempts;
    }

    @Override
    @JsonProperty
    public User getUser() {
        return user;
    }

    @Override
    @JsonProperty
    public int getNumberOfAttempts() {
        return attempts;
    }

    @Override
    public String toString() {
        return user.getName() + ": " + attempts + " attempts";
    }
}
