package io;

/**
 * Created by Victoria on 21.02.2017.
 */
public class Userino implements User {
    private int id;
    private String name;
    private String password;

    public Userino(int id, String name, String password){
        this.id = id;
        this.name = name;
        this.password = password;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return id + ", " + name + ", " + password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Userino userino = (Userino) o;

        if (id != userino.id) return false;
        if (name != null ? !name.equals(userino.name) : userino.name != null) return false;
        return password != null ? password.equals(userino.password) : userino.password == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
