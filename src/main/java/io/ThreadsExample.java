package io;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Victoria on 23.02.2017.
 */
public class ThreadsExample {


    public static void main(String[] args) throws InterruptedException {


        ExecutorService executorService = Executors.newFixedThreadPool(4);


        IntStream.range(0, 4)
                .mapToObj(i -> (Runnable) ThreadsExample::heavyAction)
                .collect(Collectors.toList());


        CountDownLatch countDownLatch = new CountDownLatch(1);


        countDownLatch.await();

//        heavyAction();
        System.out.println("blyad");
    }

    private static void heavyAction() {
        while (condition()) {
            System.out.println("suka from: " + Thread.currentThread());
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean condition() {
        return true;
    }
}
