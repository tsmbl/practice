package io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PasswordCrackerParallel implements PasswordCracker {
    @Override
    public List<PasswordCrackingResult> crack(List<User> users) {
        List<PasswordCrackingResult> list = Collections.synchronizedList(new ArrayList<>());
        SingleUserPasswordCracker singleCracker = new SingleUserPasswordCrackerParralel();
        users
                .parallelStream()
                .forEachOrdered(u -> list.add(singleCracker.crack(u)));
        return list;
    }
}
