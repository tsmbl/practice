package io;

/**
 * User model
 */
public interface User {

  int getId();

  String getName();

  String getPassword();

}
