package io;

import com.mysql.fabric.jdbc.FabricMySQLDriver;
import com.typesafe.config.Config;

import java.sql.*;
import java.util.Collection;

/**
 * Created by Victoria on 25.02.2017.
 */
public class PasswordCrackingResultReportWriterToSQL implements PasswordCrackingResultReportWriter {

    private String url;
    private String dbName;
    private String user;
    private String password;

    public PasswordCrackingResultReportWriterToSQL(Config config) {
        url = config.getString("database.url");
        dbName = config.getString("database.name");
        user = config.getString("database.user");
        password = config.getString("database.password");
    }


    @Override
    public void writeReport(Collection<PasswordCrackingResult> passwordCrackingResults) {

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            createTable(connection);
            String insertTableSQL = "INSERT INTO " + dbName + " (id, name, password, attempts) VALUES (?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(insertTableSQL);
            for (PasswordCrackingResult result : passwordCrackingResults) {

                statement.setInt(1, result.getUser().getId());
                statement.setString(2, result.getUser().getName());
                statement.setString(3, result.getUser().getPassword());
                statement.setInt(4, result.getNumberOfAttempts());

                statement.executeUpdate();
                System.out.println("Report writing to Database finished!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void createTable(Connection connection) throws SQLException {
        String createTable = "CREATE TABLE IF NOT EXISTS " + dbName + "(" +
                "  `id` INT NOT NULL," +
                "  `name` VARCHAR(45) NULL," +
                "  `password` VARCHAR(45) NULL," +
                "  `attempts` INT NULL," +
                "  PRIMARY KEY (`id`))";
        Statement creationStatement = connection.createStatement();
        creationStatement.execute(createTable);
    }
}
