package io;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Victoria on 21.02.2017.
 */
public class ListFiles {
    public List<File>  listAllFilesAndDirectories(String directoryName){
        File directory = new File(directoryName);
        List<File> list = new ArrayList<>(Arrays.asList(directory.listFiles()));
        return list;
    }
}

