package io;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;

import java.io.File;
import java.io.IOException;
import java.util.Collection;


public class PasswordCrackingResultReportWriterToJson implements PasswordCrackingResultReportWriter {
    Config config;

    public PasswordCrackingResultReportWriterToJson(Config config) {
        this.config = config;
    }

    @Override
    public void writeReport(Collection<PasswordCrackingResult> passwordCrackingResults) {
        ObjectMapper mapper = new ObjectMapper();
        String pathname = config.getString("reportFiles.json");
        File file = new File(pathname);

        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, passwordCrackingResults);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Report writing to JSON finished!");

    }
}
