package io;

import com.typesafe.config.Config;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;


public class PasswordCrackingResultReportWriterToCsv implements PasswordCrackingResultReportWriter {
    private Config config;

    public PasswordCrackingResultReportWriterToCsv(Config config) {
        this.config = config;
    }

    @Override
    public void writeReport(Collection<PasswordCrackingResult> passwordCrackingResults) {
        String pathname = config.getString("reportFiles.csv");
        try (FileWriter w = new FileWriter(pathname)){
            w.write("id,name,password,attempts" + "\n");
            for (PasswordCrackingResult result : passwordCrackingResults) {
                w.write(passCrackingResultToCommaSeparatedString(result) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Report writing to CSV finished!");

    }

    private String passCrackingResultToCommaSeparatedString(PasswordCrackingResult result) {
        User user = result.getUser();
        return user.getId() + "," + user.getName() + "," + user.getPassword() + "," + result.getNumberOfAttempts();
    }

}
