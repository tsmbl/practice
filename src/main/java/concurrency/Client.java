package concurrency;

/**
 * Created by Victoria on 12.02.2017.
 */
public interface Client {

    void getAndPutBack(int amount);

}
