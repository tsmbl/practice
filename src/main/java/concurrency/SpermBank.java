package concurrency;

import java.util.Random;

/**
 * Created by Victoria on 12.02.2017.
 * 1) synchronized method java
 * 2) Atomic variables java
 */
public class SpermBank implements Bank {

    private volatile int money;

    public SpermBank(int money) {
        this.money = money;
    }


    @Override
    public synchronized void put(int amount) {
            money += amount;
    }

    @Override
    public synchronized void get(int amount) {
            money -= amount;
    }

    public synchronized int getMoney() {
            return money;
    }
}

class Klient implements Client, Runnable {
    Random random = new Random();
    Bank bank;

    public Klient(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void getAndPutBack(int amount) {
        for (int i = 0; i < 100000; i++) {
            bank.get(amount);
            bank.put(amount);
        }
    }

    @Override
    public void run() {
        getAndPutBack(random.nextInt(5));
    }
}
