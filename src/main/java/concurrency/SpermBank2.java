package concurrency;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Victoria on 12.02.2017.
 * 1) synchronized method java
 * 2) Atomic variables java
 */
public class SpermBank2 implements Bank {
    private AtomicInteger money;

    public SpermBank2(int money) {
        this.money = new AtomicInteger(money);
    }



    @Override
    public void put(int amount) {
        money.addAndGet(amount);
    }

    @Override
    public void get(int amount) {
        money.addAndGet(-amount);
    }

    public int getMoney() {
        return money.get();
    }
}

