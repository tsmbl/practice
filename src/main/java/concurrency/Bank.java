package concurrency;

/**
 * Created by Victoria on 12.02.2017.
 */
public interface Bank {

    void put(int amount);

    void get(int amount);
}
