package concurrency;

import javax.swing.event.HyperlinkEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Victoria on 12.02.2017.
 */
class Huy implements Runnable {

    @Override
    public void run() {
        System.out.println("Huy!");
    }
}

class Pizda implements Runnable {

    @Override
    public void run() {
        System.out.println("Pizda!");
    }
}

class Ebal implements Runnable {

    @Override
    public void run() {
        System.out.println("Ebal v rot!");
    }
}

class Blyad implements Runnable {

    @Override
    public void run() {
        System.out.println("Blyad'!");
    }
}


public class BadLanguage {
    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        executorService.submit(new Blyad());
        executorService.submit(new Ebal());
        executorService.submit(new Huy());
        executorService.submit(new Pizda());
        executorService.shutdown();

//        List<Thread> list = new ArrayList<Thread>(){
//            {
//                add(new Thread(new Huy()));
//                add(new Thread(new Pizda()));
//                add(new Thread(new Ebal()));
//                add(new Thread(new Blyad()));
//            }
//        };
//        for (Thread t : list){
//            t.start();
//        }


//        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
//
//        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("hui"), 0L, 1L, TimeUnit.SECONDS);

//        while (true) {
//
//
//            doSmth();
//            Thread.sleep(n`);
//        }
//

    }

    private static void doSmth() {


    }
}
