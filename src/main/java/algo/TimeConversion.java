package algo;

import java.util.*;
import java.text.*;

/**
 * Created by Victoria on 20.01.2017.
 */
public class TimeConversion {

        public static void main(String[] args) throws ParseException{
           // Scanner in = new Scanner(System.in);
            //String time = in.next();
            String time = "07:05:45PM";
            DateFormat formatter = new SimpleDateFormat("hh:mm:ssa", Locale.US);
            Date date = formatter.parse(time);
            DateFormat newFormatter = new SimpleDateFormat("HH:mm:ss");
            System.out.println(newFormatter.format(date));
        }
    }

