package algo;

import java.util.*;

public class DiagonalDifference {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[][] = new int[n][n];
        int sumPrimary = 0;
        int sumSecondary = 0;
        for(int i=0; i < n; i++){
            for(int j=0; j < n; j++){
                a[i][j] = in.nextInt();
                if (i == j){
                    sumPrimary+=a[i][j];
                }
                if (i + j + 1 == n){
                    sumSecondary+=a[i][j];
                }
            }
        }
        System.out.println(Math.abs(sumPrimary-sumSecondary));
    }
}

