package algo;

import java.util.*;

/**
 * Created by Victoria on 19.01.2017.
 */
public class CompareTheTriplets {


        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int a0 = in.nextInt();
            int a1 = in.nextInt();
            int a2 = in.nextInt();
            int b0 = in.nextInt();
            int b1 = in.nextInt();
            int b2 = in.nextInt();
            int[] alice = {a0, a1, a2};
            int[] bob = {b0, b1, b2};
            int aliceScore = 0;
            int bobScore = 0;
            for (int i = 0; i < 3; i++){
                if (alice[i] > bob [i]){
                    aliceScore++;
                } else if (bob[i]>alice[i]){
                    bobScore++;
                }
            }
            System.out.println(aliceScore + " " + bobScore);
        }
    }

