package tdd;

/**
 * Created by Victoria on 12.02.2017.
 */
public class Mamka {
    private String state;

    public Mamka(String state) {

        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
